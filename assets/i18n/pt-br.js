{
    "header": {
        "home": "Home",
        "kmm": "KMM",
        "practice_areas": "Áreas de Atuação",
        "team": "Equipe",
        "contact": "Contato"
    },
    "member": {
        "title": "Equipe",
        "profile": "Sobre",
        "view_profile": "Ver Perfil",
        "professional_exercise": "Atuação"
    },
    "kmma": {
        "about": "Sobre",
        "text": "<p>KÜNZLI, MORAES E MAGGI ADVOGADOS nasceu da união de advogados experientes na área do direito empresarial e internacional, focados no comprometimento com a ética, eficiência e soluções jurídicas sob medida para cada cliente. Nossa busca por resultado atende aos pedidos de nossos clientes, mas sabemos que não é suficiente. O alto padrão ético é fundamental para manter negócios sustentáveis e a perenidade das instituições.</p><p>Nossos clientes são empresas atuantes nos mais diversos mercados, tanto nacionais quanto estrangeiras. Os projetos da área internacional envolvem tanto empresas estrangeiras com disposição para investir e explorar o mercado brasileiro, como empresas nacionais com interesse na sua internacionalização.</p><p>A atuação de KMM Advogados extrapola a mera prestação de serviços jurídicos e torna seus profissionais verdadeiros conselheiros das empresas, pois o dinamismo da economia atual exige que o advogado entenda com profundidade dos negócios desenvolvidos por seus clientes.</p><p>Nesse sentido, nossos advogados possuem larga experiência internacional, com formação em faculdades nacionais e estrangeiras de excelência e licenciados para prática do direito em múltiplas jurisdições. Os profissionais estudam a fundo o mercado de atuação dos clientes para dar a devida atenção a cada detalhe que determina o sucesso dos negócios.</p>"
    },
    "contact_us": {
            "email": {
                "label": "E-mail",
                "value": "kmma@kmma.com.br",
                "href": "mailto:kmma@kmma.com.br"
            },
            "phone": {
                "label": "Telefone",
                "value": "+55 11 3060.5200",
                "href": "tel:+551130605200"
            },
            "address": {
                "label": "Endereço",
                "value": "Alameda Ministro Rocha Azevedo, 456 • 11º andar<br>Cerqueira César • São Paulo, SP • Brasil • CEP: 01410-000"
            }
    },
    "footer": {
        "signup_newsletter": "Inscreva-se para receber nosso boletim informativo",
        "form": {
            "name": "Nome",
            "email": "E-mail",
            "signup": "Inscrever <i class='ico-paper-plane'></i>"
        },
        "enus": "EN",
        "ptbr": "PT",
        "credits": "créditos",
        "address": "<i class='ico-location-pin'></i> Alameda Ministro Rocha Azevedo, 456 • 11º andar, Cerqueira César • São Paulo, SP • Brasil • CEP 01410-000 • +55 11 3060.5200"
    },
    "slides":[
        {
            "id": "slide-002",
            "src": "/img/sl2.jpg",
            "title": ""
        }
    ],
    "slideCurrent": 0,
    "slideTimeout": 5000,
    "slideFn": null,
    "abouts":[
        {
            "title": "Projetos específicos para soluções de problemas de alta complexidade.",
            "description": "<p>A atuação de KMM Advogados extrapola a mera prestação de serviços jurídicos e torna seus profissionais verdadeiros conselheiros das empresas, pois o dinamismo da economia atual exige que o advogado entenda com profundidade dos negócios desenvolvidos por seus clientes.</p>",
            "img": "/img/about-001.jpg"
        },{
            "title": "Alto comprometimento com a ética, eficiência e resultado.",
            "description": "<p>A atuação de ....</p>",
            "img": "/img/about-002.jpg"
        },{
            "title": "Foco em defesa da concorrência, estruturações societárias e contratos empresariais.",
            "description": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>",
            "img": "/img/about-003.jpg"
        }
    ],
    "aboutCurrent": 0,
    "practices": [
        {
            "title": "Reparação de danos econômicos",
            "description": "",
            "atributes": [
                "Ações de reparação de danos por danos decorrentes de cartel, concorrência desleal, abuso de posição dominante e outras violações concorrenciais",
                "Recuperação de ativos na área concorrencial"
            ],
            "img": "img/competition-damages-litigation.svg",
            "mobile_img": "img/m-competition-damages-litigation.svg",
            "color": "#ffffff"
        },
        {
            "title": "Proteção de Sócio e Investidores",
            "description": "",
            "atributes": [
                "Constituição de empresas",
                "Acompanhamento da vida societária de empresas Ltda. e S.A.",
                "Solução de conflitos entre sócios",
                "Atuação perante agências reguladoras",
                "Reestruturações para otimização de estrutura tributária",
                "Reorganização societária envolvendo empresas estrangeiras (cross border transactions)",
                "Reestruturação sucessória",
                "Elaboração e revisão de contratos em geral",
                "Auxílio na fase de negociação dos contratos",
                "Contratos internacionais",
                "MoU, LoI e demais contratos preliminares",
                "Contratos com a administração pública (licitações)"
            ],
            "img": "img/defense-of-shareholders-and-investors.svg",
            "mobile_img": "img/m-defense-of-shareholders-and-investors.svg",
            "color": "#ffffff"
        },
        {
            "title": "Concorrencial e Compliance",
            "description": "",
            "atributes": [
                "Notificação de atos de concentração",
                "Impugnação a atos de concentração",
                "Implementação de programas de compliance antitruste",
                "Consultas ao Conselho Administrativo de Defesa Econômica - CADE",
                "Defesa em processos administrativos",
                "Celebração de acordos de leniência, leniência plus, TCC e TAC",
                "Denúncia de infrações à ordem econômica"
            ],
            "img": "img/antitrust-and-compliance.svg",
            "mobile_img": "img/m-antitrust-and-compliance.svg",
            "color": "#ffffff"
        },
        {
            "title": "Internacional Estratégico",
            "description": "",
            "atributes": [
                "Investimentos de brasileiros no exterior",
                "Recepção a estrangeiros que desejam investir no Brasil",
                "Atuação perante o Banco Central do Brasil para registro de operações",
                "Negociação de contratos internacionais com aplicação das principais legislações estrangeiras",
                "Ampla rede de escritórios parceiros no exterior",
                "Atuação perante câmaras de comércio e agências de fomento a investimento internacional",
                "Desenvolvimento de Estratégias e Administração de Litígios em Múltiplas Jurisdições",
                "Resolução de Litígios Nacionais e Internacionais",
                "Atuação perante câmaras arbitrais no Brasil e no exterior",
                "Reconhecimento de sentenças estrangeiras no Brasil"
            ],
            "img": "img/strategic-international-advisory-and-litigation.svg",
            "mobile_img": "img/m-strategic-international-advisory-and-litigation.svg",
            "color": "#ffffff"
        }
    ],
    "practiceCurrent": 0,
    "members": [
        {
            "photo": "img/photo-profile-bruno-maggi.png",
            "thumb": "img/thumb-profile-bruno-maggi.png",
            "name": "Bruno Oliveira MAGGI",
            "job": "Sócio",
            "description": "<p>Bruno Maggi é pioneiro no desenvolvimento de ações de indenização contra danos gerados por cartéis no Brasil, tendo celebrado acordos para seus clientes e atualmente o escritório patrocina dezenas de prejudicados contra cartéis em diferentes mercados.<br> Bruno Maggi leciona como professor assistente no curso de Graduação da Faculdade de Direito da Universidade de São Paulo (USP); nos de pós-graduação do CEU Law School, onde é coordenador da cadeira de direito antitruste e membro também do departamento de direito civil; no curso de graduação da Escola de Economia de São Paulo – EESP-FGV. É ainda Assessor do Tribunal de Ética e Disciplina da Seção de São Paulo da Ordem dos Advogados do Brasil. Diretor-tesoureiro do YL Committee da International Bar Association (IBA). Inscrito na Ordem dos Advogados do Brasil – Seção de São Paulo (OAB/SP). Membro da Associação dos Advogados de São Paulo (AASP). Autor de livro e diversos artigos no Brasil e no exterior.</p><h4>Formação Acadêmica</h4><ul><li>Doutorando em Direito Civil pela Faculdade de Direito da Universidade de São Paulo (USP);</li><li>Mestre em Direito Civil (aprovado com distinção com a dissertação sobre dano resultante de cartéis) pela Faculdade de Direito da Universidade de São Paulo (USP), 2010;</li><li>Bacharel em Direito pela Faculdade de Direito da Universidade de São Paulo (USP), 2005.</li></ul><h4>Associações</h4><ul><li>Inscrito na Ordem dos Advogados do Brasil – São Paulo (OAB/SP)</li><li>Associação dos Advogados de São Paulo (AASP)</li><li>Assessor do Tribunal de Ética e Disciplina da Seção de São Paulo da Ordem dos Advogados do Brasil</li><li>Diretor-tesoureiro do YL Committee da International Bar Association (IBA)</li></ul><h4>Reconhecimentos</h4><ul><li>Especialista jurídico (assessor da corte) para direito brasileiro em Hong Kong, 2014</li><li>Agraciado com o Prix Des Anciens Présidents (Prêmio dos Antigos Presidentes) da Association</li><li>Internationale des Jeunes Avocats (AIJA) do 48º Congresso Anual, Charleston, EUA</li></ul><h4>Idiomas</h4><ul><li>Português, inglês e espanhol</li></ul>",
            "linkedin": "https://br.linkedin.com/in/bruno-oliveira-maggi-a281399",
            "area": "<p>Bruno Maggi possui experiência na atuação perante os órgãos do Sistema Brasileiro de Defesa da Concorrência (CADE). Sua atuação abrange até os mais complexos mercados relevantes, sendo experiente na Notificação e Impugnação de Atos de Concentração, defesa de pessoas físicas e jurídicas em Processos Administrativos, negociação de Leniência e Acordos de Cessação de Conduta, elaboração de Denúncias contra infrações à ordem econômica e realização de Consultas ao CADE.</p> <p>Atua nas áreas de Defesa da Concorrência e Direito Econômico; Direito Societário e Contratual; Direito Administrativo e Regulatório; e Defesa do Consumidor.</p>",
            "mail": "bmaggi@kmma.com.br",
            "vcard": null
        },{
            "photo": "img/photo-profile-willi-kunzli.png",
            "thumb": "img/thumb-profile-willi-kunzli.png",
            "name": "Willi Sebastian KÜNZLI",
            "job": "Sócio",
            "area": "<p>Willi Künzli possui sua atuação voltada para as áreas de direito comercial (contratos, reorganizações societárias e fusões e aquisições), bem como forte especialização em contencioso cível e arbitragem.</p> <p>Na área contenciosa sua atuação tem se destacado em casos de reparação de danos, em especial por infrações concorrenciais ou societárias. Por sua experiência e formação internacional, ele tem frequentemente atuado em litígios complexos envolvendo jurisdições estrangeiras. Willi é licenciado para a prática do direito no Brasil e no Estado de Nova Iorque (EUA).</p>",
            "linkedin": "https://br.linkedin.com/in/willi-k%C3%BCnzli-57ba3322",
            "description": "<p>Willi Künzli foi nomeado como advogado de excelência em direito societário e fusões e aquisições, por diretores jurídicos das principais empresas da América Latina, pertencentes à The Latin American Corporate Counsel Association (2016-2019).</p><h4>Formação Acadêmica</h4><ul><li>Doutorando em Direito Internacional pela Faculdade de Direito da Universidade de São Paulo (USP);</li><li>Mestre em Direito Internacional pela Faculdade de Direito da Universidade de São Paulo (USP), 2015;</li><li>LL.M. pela University of California Berkeley, School of Law, 2014;</li><li>Especialização em Direito Empresarial pela Fundação Getúlio Vargas – São Paulo Escola de Direito (FGV/GVLaw), 2011;</li><li>Bacharel em Direito pela Faculdade de Direito da Universidade Presbiteriana Mackenzie, 2008.</li></ul><h4>Associações</h4><ul><li>Inscrito na Ordem dos Advogados do Brasil – São Paulo (OAB/SP) e Rio de Janeiro (OAB/RJ);</li><li>New York Court – admitido para prática do direito no Estado de Nova Iorque, Estados Unidos;</li><li>Associação dos Advogados de São Paulo (AASP);</li><li>Assessor do Tribunal de Ética e Disciplina da Seção de São Paulo da Ordem dos Advogados do Brasil;</li><li>American Bar Association (ABA);</li><li>New York State Bar Association (NYSBA).</li></ul><h4>Reconhecimentos</h4><ul><li>Desde 2016 tem sido nomeado todo ano como advogado recomendado em Societário e M&amp;A pela Latin American Corporate Counsel Association (LACCA)</li></ul><h4>Idiomas</h4><ul><li>Português, Inglês e Alemão</li></ul>",
            "mail": "wkunzli@kmma.com.br",
            "vcard": null
        },{
            "photo": "img/photo-profile-fernando-faina.png",
            "thumb": "img/thumb-profile-fernando-faina.png",
            "name": "Fernando FAINA",
            "job": "Advogado Associado",
            "linkedin": "https://www.linkedin.com/in/fernando-faina-41412aa4/",
            "description": "<p>Fernando Faina está inscrito na ordem dos advogados do brasil – secção São Paulo (OAB/SP). Ele é autor de artigos em periódicos brasileiros.</p><h4>Formação Acadêmica</h4><ul><li>Bacharel em direito pela Pontifícia Universidade Católica de São Paulo (PUC-SP), 2014.</li></ul><h4>Idiomas</h4><ul><li>Português e Inglês.</li></ul>",
            "area": "<p>Fernando Faina trabalhou em grandes escritórios nas áreas de direito público, antitruste e concorrencial. Antes de integrar o escritório, foi aluno da escola de formação da sociedade brasileira de direito público (EF/SBDP) e também estagiário pesquisador na FGV Direito SP, integrando o projeto supremo em pauta.</p>",
            "mail": "ffaina@kmma.com.br",
            "vcard": null
        },{
            "photo": "img/photo-profile-denis-scarpato.png",
            "thumb": "img/thumb-profile-denis-scarpato.png",
            "name": "Denis SCARPATO",
            "job": "Advogado Associado",
            "title": null,
            "area": "<p>Denis Scarpato atua na área de Direito Societário (contencioso e consultivo), com ênfase em M&amp;A, contratos, reestruturação societária e planejamento sucessório. Ele frequentemente auxilia clientes internacionais na condução de seus negócios no Brasil.</p>",
            "education": null,
            "practices": null,
            "languages": null,
            "memberships": null,
            "linkedin": "https://br.linkedin.com/in/denis-scarpato-a24871102",
            "description": "<p>Denis Scarpato está inscrito na Ordem dos Advogados do Brasil – Secção São Paulo (OAB/SP)</p><h4>Formação Acadêmica</h4><ul><li>Bacharel em Direito pela Universidade Presbiteriana Mackenzie, 2016.</li></ul><h4>Idiomas</h4><ul><li>Português, Inglês e Italiano.</li></ul>",
            "mail": "dscarpato@kmma.com.br",
            "vcard": null

        },{
            "photo": "img/photo-profile-isadora-aguiar.png",
            "thumb": "img/thumb-profile-isadora-aguiar.png",
            "name": "Isadora AGUIAR",
            "job": "Acadêmica de direito",
            "title": null,
            "area": "",
            "education": null,
            "practices": null,
            "languages": null,
            "memberships": null,
            "linkedin": null,
            "description": "<p>Acadêmica de Direito na Universidade Presbiteriana Mackenzie.</p><p>Isadora Aguiar presta apoio aos advogados do escritório nas áreas societária, contratual e fusões e aquisição (M&amp;A).</p>",
            "mail": "iaguiar@kmma.com.br",
            "vcard": null
        }
    ],
    "membersLeft": 0
}
