{
    "header": {
        "home": "Home",
        "kmm": "KMM",
        "practice_areas": "Practice Areas",
        "team": "Team",
        "contact": "Contact Us"
    },
    "member": {
        "title": "Team",
        "profile": "About",
        "view_profile": "View profile",
        "professional_exercise": "Professional exercise"
    },
    "kmma": {
        "about": "About",
        "text": "<p>KÜNZLI, MORAES E MAGGI ADVOGADOS resulted from the union of experienced attorneys in corporate and international law, focused on a high commitment to ethics, efficiency and legal solutions tailor made for each client.</p><p>Our focus on efficiency and results addresses the demand of our clients, but we know that this is not enough. The high ethical standard is key to maintaining sustainable business and healthy institutions.</p><p>Our clients are companiesactive in different industriesin Braziland abroad. Our international practice involves mainly foreign companiesinvesting (or intending to invest) and developing businesses in Brazil as well as Brazilian companies with businesses abroad (inbound and outbound investments).</p><p>KMM’spractice extrapolates the mere supply of legal services and itsattorneys become actual trusted advisorsof our clients.Businesses as they are developed nowadays require lawyersthat understand in depth the clients’ industry not only from a legal, but also from a business perspective.</p><p>Our attorneys have international background and experience, including multijurisdictional practices. Their education was pursued in the most reputable universities. They are frequently publishing articles and chapter as well as given lectures on important topics of law in national and international events and publications.</p><p>KMM’s attorneys has a deep understanding of each client’s businessand market to give the proper attention to every detail that determines business success.</p>"
    },
    "contact_us": {
            "email": {
                "label": "E-mail",
                "value": "kmma@kmma.com.br",
                "href": "mailto:kmma@kmma.com.br"
            },
            "phone": {
                "label": "Phone",
                "value": "+55 11 3060.5200",
                "href": "tel:+551130605200"
            },
            "address": {
                "label": "Address",
                "value": "Alameda Ministro Rocha Azevedo, 456 • 11º floor<br>Cerqueira César • São Paulo, SP • Brazil • Zipcode: 01410-000"
            }
    },
    "footer": {
        "signup_newsletter": "Subscribe to receive own newsletter",
        "form": {
            "name": "Name",
            "email": "E-mail",
            "signup": "Subscribe <i class='ico-paper-plane'></i>"
        },
        "enus": "EN",
        "ptbr": "PT",
        "credits": "credits",
        "address": "<i class='ico-location-pin'></i> Alameda Ministro Rocha Azevedo, 456 • 11º floor, Cerqueira César • São Paulo, SP • Brazil • Zipcode 01410-000 • +55 11 3060.5200"
    },
    "slides":[
        {
            "id": "slide-002",
            "src": "/img/sl2.jpg",
            "title": ""
        }
    ],
    "slideCurrent": 0,
    "slideTimeout": 5000,
    "slideFn": null,
    "abouts":[
        {
            "title": "Projetos específicos para soluções de problemas de alta complexidade.",
            "description": "<p>A atuação de KMM Advogados extrapola a mera prestação de serviços jurídicos e torna seus profissionais verdadeiros conselheiros das empresas, pois o dinamismo da economia atual exige que o advogado entenda com profundidade dos negócios desenvolvidos por seus clientes.</p>",
            "img": "/img/about-001.jpg"
        },{
            "title": "Alto comprometimento com a ética, eficiência e resultado.",
            "description": "<p>A atuação de ....</p>",
            "img": "/img/about-002.jpg"
        },{
            "title": "Foco em defesa da concorrência, estruturações societárias e contratos empresariais.",
            "description": "<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>",
            "img": "/img/about-003.jpg"
        }
    ],
    "aboutCurrent": 0,
    "practices": [
        {
            "title": "Competition damages litigation",
            "description": "",
            "atributes": [
                "Competition damage cases resulting from cartels, unfair competition, abuse of dominance and other violations",
                "Private Antitrust Enforcement",
                "Defense against Anti-competitive practices"
            ],
            "img": "img/competition-damages-litigation.svg",
            "mobile_img": "img/m-competition-damages-litigation.svg",
            "color": "#fff"
        },
        {
            "title": "Defense of shareholders and investors",
            "description": "",
            "atributes": [
                "Assistance in clients’ daily corporate matters (from initial incorporation to winding up procedures)",
                "Resolution of conflict involving stakeholders",
                "Assistance with regulatory agencies",
                "Corporate Restructuring for tax optimization",
                "Corporate reorganization involving foreign companies (cross border transactions)",
                "Succession and inheritance restructuring",
                "Draft and review of contracts",
                "Assistance with negotiation of contracts",
                "International contracts",
                "MoU, Lol and other preliminary agreements",
                "Contracts with public entities",
                "Long-term Supply Agreements",
                "EPC, EPS, built-to-suit and other construction, engineering and supply agreements",
                "Licensing agreements, royalties, SaaS and others technology contracts"
            ],
            "img": "img/defense-of-shareholders-and-investors.svg",
            "mobile_img": "img/m-defense-of-shareholders-and-investors.svg",
            "color": "#fff"
        },
        {
            "title": "Antitrust and compliance",
            "description": "",
            "atributes": [
                "Clearance of concentration acts (requests and filings)",
                "Oppositions to concentration acts",
                "Draft and implementation of antitrust compliance programs",
                "Consultation with the Administrative Council for Economic Defense – CADE on regulatory aspects",
                "Defense in administrative procedures and investigations of competition violation",
                "Representation of clients in leniency agreements and leniency plus, cease and desist agreements as well as other forms of agreement with regulators",
                "Filling of opposition against competition violation investigation and procedures"
            ],
            "img": "img/antitrust-and-compliance.svg",
            "mobile_img": "img/m-antitrust-and-compliance.svg",
            "color": "#fff"
        },
        {
            "title": "Strategic international advisory and litigation",
            "description": "",
            "atributes": [
                "Brazilian investments abroad",
                "Assistance to foreign investors in Brazil",
                "Assistance with issues involving registration with the Brazilian Central Bank",
                "Negotiation of multijurisdictional contracts",
                "Wide network of foreign partner offices",
                "Interface with chambers of commerce and development agencies",
                "Strategic development and management of multijurisdictional litigation",
                "National and International Dispute Resolution",
                "National and international arbitration (with arbitration chambers in Brazil and abroad)",
                "Enforcement in Brazil of foreign awards and judgments"
            ],
            "img": "img/strategic-international-advisory-and-litigation.svg",
            "mobile_img": "img/m-strategic-international-advisory-and-litigation.svg",
            "color": "#fff"
        }
    ],
    "practiceCurrent": 0,
    "members": [
        {
            "photo": "img/photo-profile-bruno-maggi.png",
            "thumb": "img/thumb-profile-bruno-maggi.png",
            "name": "Bruno Oliveira MAGGI",
            "job": "Partner ",
            "title": "Mestre em Direito Civil, Bacharel em Direito",
            "description": "<p>Bruno is a pioneer in the development of cartel damage claims in Brazil. He represented clients in settlement negotiations the law firm currently represents dozens of injured companies against several cartels in different markets.</p><p>Bruno Maggi is a professor of civil and competition law at the following institutions: University of São Paulo, School of Law (USP) – professor of undergraduate students, CEU Law School in São Paulo – chair of antitrust of the Corporate LL.M. course, Economy School of São Paulo (EESP-FGV) – professor of undergraduate students. He is also an assistant to the Ethics Tribunal of São Paulo Bar Association, Treasurer Director of the YL Committee of the International Bar Association (IBA). He is a member of the Brazilian Bar Association – São Paulo Section (OAB-SP), Sao Paulo Association of Lawyers (AASP). He is an author of book and articles in law in Brazil and abroad. </p><h4>Education</h4><ul><li><strong>PhD candidate</strong> in civil law at the University of Sao Paulo, School of Law;</li><li><strong>Masters’ Degree</strong> in civil law (summa cum laude) at the University of São Paulo School of Law, 2010;</li><li><strong>Degree</strong> in Law at the University of Sao Paulo School of Law, 2005</li></ul><h4>Member</h4><ul><li>Brazilian Bar Association – Sao Paulo (OAB/SP)</li><li>Sao Paulo Layers Association (AASP)</li><li>Assistant of Ethics Tribunal of the Sao Paulo Section of the Brazilian Bar Association</li><li>Treasurer Director of the YL Committee of the International Bar Association (IBA)</li></ul><h4>Achievements</h4><ul><li>Brazilian legal expert (court assistant) in Hong Kong, 2014</li><li>Awarded with the Prix Des Anciens Présidents (Past President's Award) of Association Internationale des Jeunes Avocats (AIJA) on the 48th Annual Congress, Charleston, USA</li></ul><h4>Languages</h4><ul><li>Portuguese, English and Spanish</li></ul>",
            "education": null,
            "practices": null,
            "languages": null,
            "memberships": null,
            "linkedin": "https://br.linkedin.com/in/bruno-oliveira-maggi-a281399",
            "area": "<p>Bruno Maggi is very experienced acting before the Brazilian Antitrust Authorities (CADE). His practice involves the most complex markets and he is experienced in filing and challenging of concentration acts, defense of companies and its officers in administrative proceedings, negotiation of Leniency Agreements and Cease and Decease Agreements, filing of complaints against violations of Antitrust Legislation and filing of Consultations before the CADE. </p><p>His practices are within economic law, competition law and defense of consumer, corporate and contracts as well as administrative and Regulatory.</p>",
            "mail": "bmaggi@kmma.com.br",
            "vcard": null
        },{
            "photo": "img/photo-profile-willi-kunzli.png",
            "thumb": "img/thumb-profile-willi-kunzli.png",
            "name": "Willi Sebastian KÜNZLI",
            "job": "Partner",
            "title": "<p>Mestre em Direito Internacional, Especialista em Direito Empresarial, Bacharel em Direito</p>",
            "description": "<p>Willi Künzli has been nominated as a recommended attorney in Corporate and M&A by general counsels of the main Latin American companies, members of the Latin American Corporate Counsel Association (2016-2019).</p><h4>Education</h4><ul><li>PhD candidate in International Law at the University of Sao Paulo School of Law (USP);</li><li>Masters’ Degree in International Law at the University of Sao Paulo School of Law (USP), 2015;</li><li>LL.M. from University of California Berkeley, School of Law, 2014;</li><li>Post-graduate degree (specialization) in Corporate Law at the Fundação Getúlio Vargas, School of Law, 2011;</li><li>Bachelor’s degree in Law from University Mackenzie in Sao Paulo, 2008.</li></ul><h4>Member</h4><ul><li>Brazilian Bar Association – Sao Paulo (OAB/SP) and Rio de Janeiro (OAB/RJ) Sections;</li><li>New York Court – admitted to practice law in the State of New York, United States;</li><li>Sao Paulo Layers Association (AASP);</li><li>Assistant of Ethics Tribunal of the Sao Paulo Section of the Brazilian Bar Association;</li><li>American Bar Association (ABA);</li><li>New York State Bar Association (NYSBA). </li></ul><h4>Achievements</h4><ul><li>Since 2016 has every year been nominated as a recommended attorney in Corporate and M&A by the Latin American Corporate Counsel Association (LACCA).</li></ul><h4>Languages</h4><ul><li>Portuguese, English and German.</li></ul>",
            "education": null,
            "practices": null,
            "languages": null,
            "memberships": null,
            "linkedin": "https://br.linkedin.com/in/willi-k%C3%BCnzli-57ba3322",
            "area": "<p>Willi Künzli represents clients in corporate law (contracts, corporate reorganization and M&A) as well as complex litigation and arbitration cases.</p><p>Willi Künzli’s practice in litigation has focuses on damage claims, mainly resulting from competition and corporate violations. Because of his international experience and education, he has frequently been involved in complex multijurisdictional litigation. Willi is licensed to practice law in Brazil (Sao Paulo and Rio de Janeiro) and in New York (USA).</p>",
            "mail": "wkunzli@kmma.com.br",
            "vcard": null
        },{
            "photo": "img/photo-profile-fernando-faina.png",
            "thumb": "img/thumb-profile-fernando-faina.png",
            "name": "Fernando FAINA",
            "job": "Associate Attorney",
            "title": "Fernando Faina is licensed to practice law in Brazil, State of Sao Paulo (OAB/SP). Fernando Faina is an author of articles in Brazilian law magazines.",
            "area": "<p>Fernando Faina is licensed to practice law in Brazil, State of Sao Paulo (OAB/SP). Fernando Faina is an author of articles in Brazilian law magazines.</p>",
            "education": null,
            "practices": null,
            "languages": null,
            "memberships": null,
            "linkedin": "https://br.linkedin.com/in/willi-k%C3%BCnzli-57ba3322",
            "description": "<p>Fernando Faina practiced law in some of Brazil’s biggest law firms in the fields of public , antitrust and competition law. Before joining the firm, he studied at the Brazilian Society of Public Law School (EF/SBDP) and was a researcher at the Fundação Getúlio Vargas School of Law, member of the “Supremo em Pauta” project, that conducted empirical research of Brazil’s Supreme Court case law.</p><h4>Education</h4><ul><li>Bachelor’s degree in law from the Pontifical Catholic University of Sao Paulo (PUC-SP), 2014.</li></ul><h4>Languages</h4><ul><li>Portuguese and English</li></ul>",
            "mail": "ffaina@kmma.com.br",
            "vcard": null
        },{
            "photo": "img/photo-profile-denis-scarpato.png",
            "thumb": "img/thumb-profile-denis-scarpato.png",
            "name": "Denis SCARPATO",
            "job": "Associate Attorney",
            "title": "Bacharel em Direito",
            "area": "<p>Denis Scarpato is licensed to practice law in Brazil, State of Sao Paulo (OAB/SP).</p>",
            "education": null,
            "practices": null,
            "languages": null,
            "memberships": null,
            "linkedin": "https://br.linkedin.com/in/denis-scarpato-a24871102",
            "description": "<p>Denis Scarpato is experienced in the field of corporate law (litigation and advisory), with emphasis on contracts, M&A, corporate reorganizations as well as succession and inheritance planning. Denis frequently advises international clients on how to conduct businesses in Brazil as well as on foreign investment matters</p><h4>Education</h4><ul><li>Bachelor’s degree in law from the University Mackenzie, 2016.</li></ul><h4>Languages </h4><ul><li>Portuguese, English and Italian.</li></ul>",
            "mail": "dscarpato@kmma.com.br",
            "vcard": null

        },{
            "photo": "img/photo-profile-isadora-aguiar.png",
            "thumb": "img/thumb-profile-isadora-aguiar.png",
            "name": "Isadora AGUIAR",
            "job": "Bachelor degree student in law",
            "title": "Bachelor degree student in law",
            "area": "",
            "education": null,
            "practices": null,
            "languages": null,
            "memberships": null,
            "linkedin": "https://br.linkedin.com/in/denis-scarpato-a24871102",
            "description": "<p>Isadora Aguiar is a bachelor student of law at the University Mackenzie in Sao Paulo.</p><p>Isadora assists our attorneys in corporate, M&A and contracts.</p><h4>Languages</h4><ul><li>Portuguese and English.</li></ul>",
            "mail": "iaguiar@kmma.com.br",
            "vcard": null
        }
    ],
    "membersLeft": 0
}
