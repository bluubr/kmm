// SET images, panels and languages in unique json
var variables = {};
var timeout;
var mySiema;
// Google Maps Options Style
var googleStyle = [];
// Language pre config
var language = {
    current: "",
    list: [
        {
            flag: "",
            name: "Português",
            slug: "pt-br",
            src: "assets/i18n/pt-br.js"
        },
        {
            flag: "",
            name: "English",
            slug: "en-us",
            src: "assets/i18n/en-us.js"
        }
    ]
};
// Global Function
var global = {
    // Initial Function
    init: function(){

        // Always bg black
        $('header').addClass('active');

        // Call slideshow at first section
        global.slideshow();

        // Call practices area modules
        global.practices();

        // Call members section
        global.members();

        // Call google maps of Contact Us panel
        global.loadGoogleStyle();

        // Call translator
        global.translate();
        $('.header-link').click(function(){
            $('.header-link').removeClass('current');
            $(this).addClass('current');
        });
        // Bind open panel module
        $('.open-panel').unbind().click(global.openPanel);

        // Bind close panel module
        $('.close-panel').unbind().click(global.closePanel);

        // Bind change language site
        $('.translate').unbind().click(global.changeLanguage);

        setTimeout(function(){
            $('.preloader').fadeOut(300)
        },1000)
    },

    // Init binds buttons
    binds: function(){
        // Call header scroll efect on load
        global.scrolling();

        // Bind header scroll efect when scroll
        $(window).scroll(global.scrolling);

        // Bind scroll to anchor
        $('a.anchor').unbind().click(global.scrollTo);

        // Bind open contact us panel
        $('.contact').unbind().click(global.openContact);

        // Bind open about panel
        $('.about').unbind().click(global.openAbout);

        $('.toggle').unbind().click(global.toggle);
    },

    toggle: function(){
        var el = $('.menu');
        if(el.hasClass('show')){
            el.removeClass('show');
            $('body').removeClass('blocked');
        }
        else{
            el.addClass('show');
            $('body').addClass('blocked');
        }
    },

    // Changing language
    changeLanguage: function(){
        var lang = $(this).data('language');
        var onlyUrl = window.location.href.replace(window.location.search,'');
        window.history.pushState("", "KMMA ", onlyUrl );

        for(var i=0; i<language.list.length; i++){
            if(language.list[i].slug == lang){
                language.current = language.list[i];
                sessionStorage.setItem('language', JSON.stringify(language.current ));
                $('html').attr('lang',lang)
            }
        }
        global.closePanel();
        global.loadLang()
    },

    // Translate every element
    translate: function(){
        $('*[data-translate]').each(function(){
            var mapString = $(this).data('translate');
            var mapObject = mapString.split('|');
            var text = global.mapIndex(variables, mapObject[0]);
            switch(mapObject[1]){
                case 'html':
                $(this).html(text)
                break;
                case 'placeholder':
                $(this).attr('placeholder',text);
                break;
            }
        });
        $('.translate').show();
        $('.'+language.current.slug).hide();
    },
    mapIndex: function(obj,is, value) {
        if (typeof is == 'string')
        return global.mapIndex(obj,is.split('.'), value);
        else if (is.length==1 && value!==undefined)
        return obj[is[0]] = value;
        else if (is.length==0)
        return obj;
        else
        return global.mapIndex(obj[is[0]],is.slice(1), value);
    },
    loadLang: function(){
        var qs = global.parseQueryString(window.location.search.substring(1));
        if(typeof qs['en-us'] != 'undefined'){
            sessionStorage.setItem('language', JSON.stringify(language.list[1]));
        }
        var loadLang = sessionStorage.getItem('language');
        if(!loadLang){

            sessionStorage.setItem('language', JSON.stringify(language.list[0]));
            language.current = language.list[0];
        }
        else{
            language.current = JSON.parse(loadLang);
        }
        var lang = language.current.src;
        $.getJSON(lang , function( json ) {
            variables = json;
        }).done(function( json ) {
            global.init();
        });
    },
    loadGoogleStyle: function(){
        $.getJSON( "assets/js/google-style.js", function( json ) {
            googleStyle = json;
        }).done(function( json ) {

        });
    },
    scrolling: function(){
        var self = window;
        // UNCOMMENT TO ACTIVE SHADOWN CLASS
        // if($(self).scrollTop() > 100 || $('.overlayer.show').length > 0) { // this refers to window
        //     $('header').addClass('active')
        // }
        // else{
        //     $('header').removeClass('active')
        // }
        $('.anchor').each(function(){
            var element = $(this).attr('href');
            var anchor = $(element).offset().top - 80;
            if($(self).scrollTop() > anchor){
                $('.header-link').removeClass('current');
                $(this).addClass('current');
            }
        })
        // var fromTop = ($(window).width > 768) ? $(window).scrollTop()/3 : $(window).scrollTop()/1.5;
        // $(".slide").css("background-position","left -"+fromTop+"px");
        $(".menu").removeClass("show");
    },
    slideshow: function(){
        var slideshowHtml = '';
        var current = ' current';
        for(var i=0; i<variables.slides.length;i++){
            slideshowHtml += '<div class="'+variables.slides[i].id + current +' slide" title="'+variables.slides[i].title+'" style="background-image: url('+variables.slides[i].src+')"></div>';
            current = '';
        }
        $('.slide-show').html(slideshowHtml);
        if(variables.slides.length > 1){
            $('.slide-show').append('<a href="javascript:void(0)" class="prev"><i class="ico-chevron-thin-left"></i></a><a href="javascript:void(0)" class="next"><i class="ico-chevron-thin-right"></i></a>');
        }
        $('.prev').unbind().click(global.prevSlide);
        $('.next').unbind().click(global.nextSlide);
        global.loop()
    },
    loop: function(){
        variables.slideFn = setTimeout(function(){
            var slideClass = '.'+variables.slides[variables.slideCurrent].id;
            $(slideClass).removeClass('current');
            variables.slideCurrent = (variables.slideCurrent < (variables.slides.length-1)) ? variables.slideCurrent+1 : 0;
            slideClass = '.'+variables.slides[variables.slideCurrent].id;
            $(slideClass).addClass('current');
            global.loop();
        },variables.slideTimeout)
    },
    nextSlide: function(){
        clearTimeout(variables.slideFn);
        var slideClass = '.'+variables.slides[variables.slideCurrent].id;
        $(slideClass).removeClass('current');
        variables.slideCurrent = (variables.slideCurrent < (variables.slides.length-1)) ? variables.slideCurrent+1 : 0;
        slideClass = '.'+variables.slides[variables.slideCurrent].id;
        $(slideClass).addClass('current');
        global.loop();
    },
    prevSlide: function(){
        clearTimeout(variables.slideFn);
        var slideClass = '.'+variables.slides[variables.slideCurrent].id;
        $(slideClass).removeClass('current');
        variables.slideCurrent = (variables.slideCurrent == 0) ? variables.slides.length-1 : variables.slideCurrent-1;
        slideClass = '.'+variables.slides[variables.slideCurrent].id;
        $(slideClass).addClass('current');
        global.loop();
    },
    practices: function(){
        var query = window.location.search.substring(1);
        var qs = global.parseQueryString(query);
        var effect = "effect-over";
        $('#practices-areas .content').html('');
        var practicesHtml = '';
        var navHtml = '';
        if(typeof qs['modelo'] != 'undefined'){
            effect = "effect-over"
        }
        var current = ' current';
        for(var i=0; i<variables.practices.length; i++){
            practicesHtml += '<article class="'+current+'" id="practices-areas-'+i+'">';
            practicesHtml += '<div class="description"><h2>'+variables.practices[i].title+'</h2>';
            practicesHtml += '<ul>';
            for(var a=0; a<variables.practices[i].atributes.length; a++){
                practicesHtml += '<li>'+variables.practices[i].atributes[a]+'</li>';
            }
            practicesHtml += '</ul></div>';
            if($(window).width() > 768){
                practicesHtml += '<figure class="'+ effect + '"><img src="'+variables.practices[i].img+'" alt="'+variables.practices[i].title+'"></figure>';
            }
            else{
                practicesHtml += '<figure class="'+ effect + '"><img src="'+variables.practices[i].mobile_img+'" alt="'+variables.practices[i].title+'"></figure>';
            }
            practicesHtml += '</article>';

            navHtml += '<li class="practices-areas-ref'+current+' practices-'+i+'" data-index="'+i+'" data-color="'+variables.practices[i].color+'">';
            navHtml += '<div class="bg" style="background-image:url('+variables.practices[i].img+')"></div>';
            navHtml += '<p>'+variables.practices[i].title+'</p>';
            navHtml += '</li>';
            current = '';
            if(i == 0){
                $('#practice-areas').css('background-color', variables.practices[i].color);
            }
        }
        $('#practices-areas .content').prepend(practicesHtml);
        $('#practices-areas .navigation').html(navHtml);
        $('.practices-areas-ref').unbind().click(global.changepractices);
    },
    changepractices: function(){
        var index = $(this).data('index');
        $('#practices-areas .navigation li, #practices-areas article').removeClass('current');
        $('#practices-areas .navigation li.practices-'+index+'').addClass('current');
        $('#practices-areas-'+index).addClass('current');
        var bgcolor = $('#practices-areas .navigation li.practices-'+index+'').attr('data-color');
        $('#practice-areas').css('background-color', bgcolor);

    },
    scrollTo: function(event){
        global.closePanel();
        // On-page links
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')&&location.hostname == this.hostname) {
            // Figure out element to scroll to
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            // Does a scroll target exist?
            if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                var headerHeight = $('header').height();
                $('html, body').animate({
                    scrollTop: target.offset().top - headerHeight
                }, 1000);
            }
        }
    },
    members: function(){
        var containerWidth = $('#staff .container').width();
        var boxQuantity = 0;
        if($(window).width() > 768){
            boxQuantity = 3
        }
        else if($(window).width() > 480){
            boxQuantity = 2
        }
        else{
            boxQuantity = 1
        }
        var boxWidth = (containerWidth/boxQuantity)-15;
        var membersHtml = '';
        // for(var n=0; n<3; n++){
        for(var i=0; i<variables.members.length; i++){
            membersHtml += '<article class="current" style="width: '+boxWidth+'px;">';
            membersHtml += '<figure class="open-panel" data-panel="member" data-user="'+variables.members[i].name+'">';
            membersHtml += '<img src="'+variables.members[i].thumb+'" alt="">';
            membersHtml += '</figure>';
            membersHtml += '<div class="description">';
            membersHtml += '<h3>'+variables.members[i].name+'</h3>';
            membersHtml += '<h4>'+variables.members[i].job+'</h4>';
            membersHtml += '<p><a href="javascript:void(0)" class="open-panel" data-panel="member" data-user="'+variables.members[i].name+'"">'+variables.member.view_profile+'</a></p>';
            membersHtml += '</div>';
            membersHtml += '</article>';
        }
        // }
        // TODO REMOVE if SIEMA
        $('.members-list').html(membersHtml);
        // global.checkPosition();
        // $('.staff .next').unbind().click(global.nextMember);
        // $('.staff .prev').unbind().click(global.prevMember);

        // let pp = 3
        mySiema = new Siema({
            selector: '.members-list',
            perPage: boxQuantity,
            loop: true,
            rtl: false,
            duration: 500,
        });
        const prev = document.querySelector('.prev');
        const next = document.querySelector('.next');

        prev.addEventListener('click', () => mySiema.prev());
        next.addEventListener('click', () => mySiema.next());
        setTimeout(global.loopMembers(),5000);
    },
    loopMembers: function(){
        timeout = setTimeout(function(){
            if ($('.members-section:hover').length == 0) {
                mySiema.next();
            }
            global.loopMembers();
        },5000);
    },
    nextMember: function(){
        variables.membersLeft = variables.membersLeft - ($(".members article").width()+20);
        $('.members').css('left',variables.membersLeft);
        setTimeout(global.checkPosition(),600);
    },
    prevMember: function(){
        variables.membersLeft = variables.membersLeft + ($(".members article").width()+20);
        $('.members').css('left',variables.membersLeft);
        setTimeout(global.checkPosition(),600);
    },
    checkPosition: function(){
        if(variables.membersLeft == 0){
            $('.staff .prev').fadeOut(300);
        }
        else{
            $('.staff .prev').fadeIn(300);
        }

        var containerWidth = $('#staff .container').width();
        var membersWidth = $('.members').width();


        if((containerWidth-membersWidth) >= variables.membersLeft){
            $('.staff .next').fadeOut(300);
        }
        else{
            $('.staff .next').fadeIn(300);
        }
    },
    openContact: function(){
        global.closePanel()
        $('body').addClass('blocked');
        $('.header-link').removeClass('current');
        $('.header-link.contact').addClass('current');
        $('#contact').addClass('show');
        $('main').addClass('inactive');
        $(document).on('keyup',function(evt) {
            if (evt.keyCode == 27) {
                global.closePanel()
            }
        });
        global.scrolling()
    },
    openAbout: function(){
        global.closePanel();
        $('body').addClass('blocked');
        $('.header-link').removeClass('current');
        $('.header-link.about').addClass('current');
        $('#about').addClass('show');
        $('main').addClass('inactive');
        $(document).on('keyup',function(evt) {
            if (evt.keyCode == 27) {
                global.closePanel()
            }
        });
        global.scrolling()
    },
    openPanel: function(){
        global.closePanel();
        $('body').addClass('blocked');
        var section = '';
        var panelHtml = '';
        $('#panel .container').fadeOut(300).html(panelHtml);
        switch($(this).data('panel')){
            case 'member':
            var user = $(this).data('user');
            for(var i=0; i<variables.members.length; i++){
                if(variables.members[i].name == user){
                    panelHtml += '<div class="row profile"><div class="col-6 profile-img" style="background-image: url('+variables.members[i].photo+')"></div><div class="col-6 scroll">';
                    panelHtml += '<h1>'+variables.members[i].name+'</h1><h3>'+variables.members[i].job+'</h3><p class="links">';
                    if(variables.members[i].linkedin != null){
                        panelHtml += '<span><a href="'+variables.members[i].linkedin+'" target="_blank"><i class="ico-linkedin"></i></a></span>';
                    }
                    if(variables.members[i].mail != null){
                        panelHtml += '<span><a href="mailto:'+variables.members[i].mail+'" target="_blank"><i class="ico-mail"></i></a></span>';
                    }
                    if(variables.members[i].vcard != null) {
                        panelHtml += '<span><a href="'+variables.members[i].vcard+'" target="_blank"><i class="ico-v-card"></i></a></span>';
                    }
                    panelHtml += '</p>'
                    // if(variables.members[i].area){
                    // panelHtml += '<div class="text-justify"><h4>'+variables.member.professional_exercise+'</h4>'+variables.members[i].area+'</div>'
                    // }
                    if(variables.members[i].description){
                        panelHtml += '<div class="text-justify">'+variables.members[i].area+variables.members[i].description+'</div>';
                    }

                    panelHtml += '</div></div>'
                }
            }
            setTimeout(function(){
                $('#panel .container').html(panelHtml).fadeIn(300);
            },1000)
            break;
        }

        $('#panel').addClass('show');
        $('.tabs a[data-tab]').unbind().click(global.changeTab);
        $(document).on('keyup',function(evt) {
            if (evt.keyCode == 27) {
                global.closePanel()
            }
        });
        $('.header-link').removeClass('current');
        $('.header-link.team').addClass('current');
        // global.scrolling();
    },
    closePanel: function(){
        $('body').removeClass('blocked');
        $('.overlayer').removeClass('show');
        setTimeout(function(){
            $('#panel .container').html('');
        },1000);
        global.scrolling();
    },
    changeTab: function(){
        $('.tab').removeClass('active');
        $('.tabs li a').removeClass('active');
        var tab = $(this).data('tab');

        $('.tab-'+tab).addClass('active');
        $(this).addClass('active');
    },
    parseQueryString: function(query) {
        var vars = query.split("&");
        var query_string = {};
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            // If first entry with this name
            if (typeof query_string[key] === "undefined") {
                query_string[key] = decodeURIComponent(value);
                // If second entry with this name
            } else if (typeof query_string[key] === "string") {
                var arr = [query_string[key], decodeURIComponent(value)];
                query_string[key] = arr;
                // If third or later entry with this name
            } else {
                query_string[key].push(decodeURIComponent(value));
            }
        }
        return query_string;
    },
    maps: function(){
        var kmm = {lat: -23.5619472, lng: -46.6601346};
        var image = {
            url: "/img/marker.svg", // url
            scaledSize: new google.maps.Size(50, 50), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(25, 50) // anchor
        };
        // The map, centered at KMM
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 15, center: kmm, styles: googleStyle});
            // The marker, positioned at KMM
            var marker = new google.maps.Marker({position: kmm, map: map, icon: image});
        }
    }
    $(document).ready(function(){
        global.loadLang();
        global.binds();
    });
